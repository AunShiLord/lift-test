﻿using System;
using System.Collections;
using System.Collections.Generic;
using Enums;
using UnityEngine;

public interface IElevator
{
    Action<int> DidReachedFloor { get; set; }
    Action<int> DidFinishedMoving { get; set; }
    Action DidStoped { get; set; }
    Action DidStartMoving { get; set; }
    Action<bool> DidChangeDoorState { get; set; }

    ElevatorDirection CurrentElevatorDirection { get; }
    bool IsDoorsOpened { get; }
    int CurrentFloor { get; }

    void Setup();
    void CallElevator(int floorNumber, ElevatorDirection elevatorDirection);
    void SendElevator(int floorNumber);
    void StopElevator();
    void CloseDoors();

}
