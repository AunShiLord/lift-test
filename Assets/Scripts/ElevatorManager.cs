﻿using System.Collections;
using System.Collections.Generic;
using Enums;
using UnityEngine;

public class ElevatorManager : MonoBehaviour
{

    private List<ElevatorFloorCell> _elevatorFloorCells;
    private List<ConsoleFloorCell> _consoleFloorCells;
    [SerializeField]
    private ElevatorFloorIndicator _elevatorFloorIndicator;
    private ElevatorStopCell _elevatorStopCell;

    [SerializeField]
    private GameObject _elevatorGO;
    private IElevator _elevator;

    private void Awake()
    {
        _elevator = _elevatorGO.GetComponent<IElevator>();
        
        _elevator.DidReachedFloor += OnElevatorDidReachFloor;
        _elevator.DidStartMoving += OnElevatorStartMoving;
        _elevator.DidStoped += OnElevatorDidStoped;
        _elevator.DidChangeDoorState += OnElevatorDidChangeDoorState;
        _elevator.DidFinishedMoving += OnElevatorFinishedMoving;
        
        _elevatorFloorIndicator.Setup();
    }

    public void Setup(int numberOfFloors, List<ConsoleFloorCell> consoleFloorCells, List<ElevatorFloorCell> elevatorFloorCells, ElevatorStopCell elevatorStopCell)
    {
        _elevator.Setup();
        _elevatorFloorIndicator.Setup();
        
        _elevatorFloorIndicator.Floor = 1;
        
        _elevatorStopCell = elevatorStopCell;
        _elevatorStopCell.elevatorStopButtonPressed += OnElevatorStopButtonPressed;
        
        _consoleFloorCells = consoleFloorCells;
        _consoleFloorCells.Reverse();
        foreach (var console in consoleFloorCells)
        {
            console.ConsoleFloorButtonPressed += OnFloorConsolePressed;
        }

        _elevatorFloorCells = elevatorFloorCells;
        _elevatorFloorCells.Reverse();
        foreach (var elevatorCell in _elevatorFloorCells)
        {
            elevatorCell.elevatorButtonPressed += OnElevatorButtonPressed;
        }

    }

    #region ElevatorInterfaceCallbacks 
    private void OnElevatorButtonPressed(int floorNumber)
    {
        _elevator.SendElevator(floorNumber);
    }

    private void OnElevatorStopButtonPressed()
    {
        _elevator.StopElevator();        
    }
    #endregion

    
    #region FloorConsoleCallback
    private void OnFloorConsolePressed(int floorNumber, ElevatorDirection direction)
    {
        _elevator.CallElevator(floorNumber, direction);
    }
    #endregion

    #region ElevatorCallbacks
    private void OnElevatorDidReachFloor(int floorNumber)
    {
        _elevatorFloorIndicator.Floor = _elevator.CurrentFloor;
    }
    private void OnElevatorDidStoped()
    {
                
        foreach (var floorCell in _elevatorFloorCells)
        {
            floorCell.TurnOffButton();
        }

        foreach (var consoleCell in _consoleFloorCells)
        {
            consoleCell.TurnOffButtons();
            consoleCell.SwitchDoorIndicator(false);
        }
        
        _elevatorStopCell.TurnOffButton();
        _elevatorFloorIndicator.TurnOffIndicators();
    }

    private void OnElevatorDidChangeDoorState(bool isDoorOpened)
    {
        _consoleFloorCells[_elevator.CurrentFloor - 1].SwitchDoorIndicator(isDoorOpened);
        if (!isDoorOpened)
        {
            _consoleFloorCells[_elevator.CurrentFloor - 1].TurnOffButtons();
            _elevatorFloorCells[_elevator.CurrentFloor - 1].TurnOffButton();
        }
    }

    private void OnElevatorFinishedMoving(int obj)
    {
        _elevatorFloorIndicator.TurnOffIndicators();
    }
    
    private void OnElevatorStartMoving()
    {
        if (_elevator.CurrentElevatorDirection == ElevatorDirection.Up)
        {
            _elevatorFloorIndicator.TurnOnUpIndicator();
        }
        else if (_elevator.CurrentElevatorDirection == ElevatorDirection.Down)
        {
            _elevatorFloorIndicator.TurnOnDownIndicator();
        }
    }
    #endregion

}
