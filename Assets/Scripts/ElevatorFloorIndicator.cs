﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElevatorFloorIndicator : MonoBehaviour
{
    [SerializeField] 
    private Text _currenFloorText;
    [SerializeField] 
    private Image _upFloorIndicator;
    [SerializeField] 
    private Image _downFloorIndicator;
    [SerializeField] 
    private Color _normalColor;
    [SerializeField] 
    private Color _highlightedColor;
    
    public int Floor
    {
        set
        {
            _floor = value;
            _currenFloorText.text = value.ToString();
        }
        get { return _floor; }
    }

    private int _floor;

    public void TurnOnUpIndicator()
    {
        TurnOffIndicators();
        _upFloorIndicator.color = _highlightedColor;
    }

    public void TurnOnDownIndicator()
    {
        TurnOffIndicators();
        _downFloorIndicator.color = _highlightedColor;
    }

    public void TurnOffIndicators()
    {
        _upFloorIndicator.color = _normalColor;
        _downFloorIndicator.color = _normalColor;
    }

    public void Setup()
    {
        Floor = 1;
        TurnOffIndicators();
    }


}
