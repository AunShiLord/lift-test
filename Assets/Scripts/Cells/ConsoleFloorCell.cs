﻿using System;
using System.Collections;
using System.Collections.Generic;
using Enums;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleFloorCell : MonoBehaviour
{
    [SerializeField] 
    private Text _cobsoleFloorText;

    [SerializeField] 
    private Toggle _consoleUpToggle;
    [SerializeField] 
    private Toggle _consoleDownToggle;

    [SerializeField] 
    private OpenDoorIndicator _openDoorIndicator;
        
    private int _floorNumber = -1;

    public int FloorNumber
    {
        private set
        {
            _floorNumber = value;
            _cobsoleFloorText.text = value.ToString();
        }
        get { return _floorNumber; }
    }

    public Action<int, ElevatorDirection> ConsoleFloorButtonPressed;

    private void Awake()
    {
        _openDoorIndicator.IsDoorOpened = false;
        
        _consoleUpToggle.onValueChanged.AddListener(OnConsoleUpValueChanged);
        _consoleDownToggle.onValueChanged.AddListener(OnConsoleDownValueChanged);
    }

    public void Setup(int floorNumber)
    {
        FloorNumber = floorNumber;
    }

    private void OnConsoleUpValueChanged(bool newValue)
    {
        if (newValue)
        {
            _consoleUpToggle.interactable = false;
            ConsoleFloorButtonPressed?.Invoke(FloorNumber, ElevatorDirection.Up);
        }
    }
    
    private void OnConsoleDownValueChanged(bool newValue)
    {
        if (newValue)
        {
            _consoleDownToggle.interactable = false;
            ConsoleFloorButtonPressed?.Invoke(FloorNumber, ElevatorDirection.Down);
        } 
    }

    public void SwitchDoorIndicator(bool isOn)
    {
        _openDoorIndicator.IsDoorOpened = isOn;
    }
    
    public void TurnOffButtons()
    {
        _consoleDownToggle.interactable = true;
        _consoleDownToggle.isOn = false;
        
        _consoleUpToggle.interactable = true;
        _consoleUpToggle.isOn = false;
    }

}