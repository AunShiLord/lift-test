﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorStopCell : BaseElevatorCell
{
    public Action elevatorStopButtonPressed;

    public override void RunButtonSpecificActionOnButtonClick()
    {
        elevatorStopButtonPressed();
    }
}
