﻿using UnityEngine;
using UnityEngine.UI;

public abstract class BaseElevatorCell : MonoBehaviour
{
 
    [SerializeField] 
    protected Toggle _elevatorCellToggle;

    private void Awake()
    {
        _elevatorCellToggle.onValueChanged.AddListener(OnValueChanged);
    }

    private void OnValueChanged(bool newValue)
    {
        if (newValue)
        {
            _elevatorCellToggle.interactable = false;
            RunButtonSpecificActionOnButtonClick();
        }
    }

    public void TurnOffButton()
    {
        _elevatorCellToggle.interactable = true;
        _elevatorCellToggle.isOn = false;
    }
    
    public abstract void RunButtonSpecificActionOnButtonClick();
    
}
