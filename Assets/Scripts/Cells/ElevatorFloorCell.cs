﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElevatorFloorCell : BaseElevatorCell
{
    [SerializeField] 
    private Text _floorText;

    private int _floorNumber = -1;
    public int FloorNumber {
        private set
        {
            _floorNumber = value;
            _floorText.text = value.ToString();
        }
        get
        {
            return _floorNumber;
        }
    }

    public Action<int> elevatorButtonPressed;


    public void Setup(int floorNumber)
    {
        FloorNumber = floorNumber;
    }

    public override void RunButtonSpecificActionOnButtonClick()
    {
        elevatorButtonPressed(FloorNumber);
    }
}
