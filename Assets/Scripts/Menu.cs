﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] 
    private GameObject _menu;
    
    [SerializeField] 
    private InputField _floorsInput;

    [SerializeField] 
    private Button _generateButton;

    [SerializeField] 
    private Button _backToMenuButton;
    
    [SerializeField] 
    private int defaultFloors = 10;
    [SerializeField] 
    private int maxFloors = 1000;

    [SerializeField] 
    private ElevatorGenerator _generator;
    
    // Start is called before the first frame update
    void Awake()
    {
        _generateButton.onClick.AddListener(GenerateElevator);
        _backToMenuButton.onClick.AddListener(ShowMenu);
        
        _floorsInput.onEndEdit.AddListener(ValidateNumbers);
    }

    private void ShowMenu()
    {
        SwitchMenu(true);
    }

    private void ValidateNumbers(string arg0)
    {        
        int floors;
        if (!int.TryParse(_floorsInput.text, out floors))
        {
            floors = 2;
        }

        floors = Mathf.Clamp(floors, 2, maxFloors);
        _floorsInput.text = floors.ToString();

    }

    private void GenerateElevator()
    {
        int floors;
        if (_floorsInput.text == String.Empty)
        {
            floors = defaultFloors;
        }
        else if (!int.TryParse(_floorsInput.text, out floors))
        {
            floors = 2;
        }

        _generator.GenerateElevator(floors);
        SwitchMenu(false);
    }
    
    private void SwitchMenu(bool isActive)
    {
        _menu.SetActive(isActive);
    }
}
