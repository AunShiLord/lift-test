﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class ElevatorGenerator : MonoBehaviour
{
    [SerializeField] 
    private ElevatorManager _elevatorManger;

    [SerializeField] 
    private Transform _consoleFloorParentTransform;
    [SerializeField] 
    private Transform _elevatorCellsParentTransform;
    
    [SerializeField] 
    private GameObject _elevatorFloorCellPrefab;
    [SerializeField] 
    private GameObject _elevatorStopCellPrefab;
    [SerializeField] 
    private GameObject _consoleFloorCellPrefab;
    [SerializeField] 
    private GameObject _fakeCellPrefab;
    
    
    // Start is called before the first frame update
    void Start()
    {
        GenerateElevator(10);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateElevator(int numberOfFloors)
    {
        ClearOldButtons();
       GameObject.Instantiate(_fakeCellPrefab, _elevatorCellsParentTransform).GetComponent<ElevatorFloorCell>();
        var newStopButton = GameObject.Instantiate(_elevatorStopCellPrefab, _elevatorCellsParentTransform).GetComponent<ElevatorStopCell>();
        GameObject.Instantiate(_fakeCellPrefab, _elevatorCellsParentTransform).GetComponent<ElevatorFloorCell>();
        // newElevatorFloorCell.Setup(i);
        
        var floorConsoles = new List<ConsoleFloorCell>();
        var elevatorFloorCells = new List<ElevatorFloorCell>();
            
        for (int i = numberOfFloors; i > 0; i--)
        {
            var newFloorConsole = GameObject.Instantiate(_consoleFloorCellPrefab, _consoleFloorParentTransform).GetComponent<ConsoleFloorCell>();
            newFloorConsole.Setup(i);
            floorConsoles.Add(newFloorConsole);

            var newElevatorFloorCell = GameObject.Instantiate(_elevatorFloorCellPrefab, _elevatorCellsParentTransform).GetComponent<ElevatorFloorCell>();
            newElevatorFloorCell.Setup(i);
            elevatorFloorCells.Add(newElevatorFloorCell);
        }

        _elevatorManger.Setup(numberOfFloors, floorConsoles, elevatorFloorCells, newStopButton);
    }

    private void ClearOldButtons()
    {
        foreach (Transform child in _consoleFloorParentTransform)
        {
            Destroy(child.gameObject);
        }
        
        foreach (Transform child in _elevatorCellsParentTransform)
        {
            Destroy(child.gameObject);
        }
    }
}
