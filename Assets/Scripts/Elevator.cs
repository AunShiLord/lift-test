﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Enums;
using UnityEngine;

public class Elevator : MonoBehaviour, IElevator
{
    public Action<int> DidReachedFloor { get; set; }
    public Action<int> DidFinishedMoving { get; set; }
    public Action DidStoped { get; set; }
    public Action DidStartMoving { get; set; }
    public Action<bool> DidChangeDoorState { get; set; }

    public ElevatorDirection CurrentElevatorDirection
    {
        get { return _currentElevatorDirection; }
    }
    public bool IsDoorsOpened 
    {
        get { return _isDoorsOpened; } 
    }
    public int CurrentFloor
    {
        get { return _currentFloor; }
    }

    private ElevatorDirection _currentElevatorDirection = ElevatorDirection.Nowhere;
    private bool _isDoorsOpened = false;
    private int _currentFloor = 1;

    private const float _elevatorSimulationTime = 1f;
    private const float _openedDoorsSimulationTime = 1f;

    private List<ElevatorDestionation> _commands = new List<ElevatorDestionation>();

    private Coroutine _openedDoorsCoroutine;
    private Coroutine _elevatorSimulationCoroutine;

    struct ElevatorDestionation
    {
        public int floor;
        public ElevatorDirection goesTo;

        public ElevatorDestionation(int newFloor, ElevatorDirection newDirection)
        {
            floor = newFloor;
            goesTo = newDirection;
        }
    }

    public void Setup()
    {
        _currentElevatorDirection = ElevatorDirection.Nowhere;
        _isDoorsOpened = false;
        _currentFloor = 1;
        StopAllCoroutines();
        _openedDoorsCoroutine = null;
        _elevatorSimulationCoroutine = null;
    }
    
    public void CallElevator(int floorNumber, ElevatorDirection elevatorDirection)
    {
        
        if (floorNumber == _currentFloor && _currentElevatorDirection == ElevatorDirection.Nowhere)
        {
            if (!_isDoorsOpened)
            {
                if (_openedDoorsCoroutine == null)
                    _openedDoorsCoroutine = StartCoroutine(SimulateOpenedDoors());
            }
                
        }
        else if (_currentElevatorDirection == ElevatorDirection.Nowhere)
        {
            _commands.Add(new ElevatorDestionation(floorNumber, elevatorDirection));
            _currentElevatorDirection = GetElevatorDirection(floorNumber, _currentFloor);
            if (_elevatorSimulationCoroutine == null)
                _elevatorSimulationCoroutine = StartCoroutine(SimulateElevatorMoving()); 
        } 
        else
            _commands.Add(new ElevatorDestionation(floorNumber, elevatorDirection));
    }

    public void SendElevator(int floorNumber)
    {
        if (floorNumber == _currentFloor && _currentElevatorDirection == ElevatorDirection.Nowhere)
        {
            if (!_isDoorsOpened)
            {
                if (_openedDoorsCoroutine == null)
                    _openedDoorsCoroutine = StartCoroutine(SimulateOpenedDoors());
            }
                
        }
        else
        {
            if (_commands.Count != 0 && GetElevatorDirection(floorNumber, _currentFloor) == _currentElevatorDirection)
            {
                _commands.Insert(0, new ElevatorDestionation(floorNumber, ElevatorDirection.Nowhere));
            }
            else
            {
                _commands.Add(new ElevatorDestionation(floorNumber, ElevatorDirection.Nowhere));
            }
        
            if (_currentElevatorDirection == ElevatorDirection.Nowhere)
            {
                _currentElevatorDirection = GetElevatorDirection(floorNumber, _currentFloor);
                if (_elevatorSimulationCoroutine == null)
                    _elevatorSimulationCoroutine = StartCoroutine(SimulateElevatorMoving());
            }
        }
            
    }

    public void StopElevator()
    {
        _commands.Clear();
        _currentElevatorDirection = ElevatorDirection.Nowhere;
        //StopAllCoroutines();
        // SendElevator(_currentFloor + (int)_currentElevatorDirection);
        StartCoroutine(SimulateOpenedDoors());
        DidStoped?.Invoke();
    }
    
    public void CloseDoors()
    {
        _isDoorsOpened = false;
        DidChangeDoorState?.Invoke(false);
    }

    private IEnumerator SimulateElevatorMoving()
    {
        DidStartMoving?.Invoke();
        yield return new WaitForSeconds(_elevatorSimulationTime);
        _currentFloor += (int) _currentElevatorDirection;
        DidReachedFloor?.Invoke(_currentFloor);

        ElevatorDestionation _commandToRemove = new ElevatorDestionation();
        var isStoped = false;

        if (_commands.Count > 0 && _commands[0].floor == _currentFloor)
        {
            if (!_commands.Exists(item => item.goesTo == _currentElevatorDirection))
            {
                _commandToRemove = _commands[0]; 
                isStoped = true;
            }
        }

        if (!isStoped)
        {
            foreach (var command in _commands)
            {
                if (command.floor == _currentFloor)
                {
                    if (command.goesTo == ElevatorDirection.Nowhere ||
                        _currentElevatorDirection == command.goesTo)
                    {
                        _commandToRemove = command; 
                        isStoped = true;
                        break;
                    }
                }
            }
        }

        _elevatorSimulationCoroutine = null;
        if (isStoped)
        {
            _commands.RemoveAll(command => command.floor == _commandToRemove.floor);
            
            if (_openedDoorsCoroutine == null )
                StartCoroutine(SimulateOpenedDoors());
        }
        else
        {
            if (_elevatorSimulationCoroutine == null )
                _elevatorSimulationCoroutine = StartCoroutine(SimulateElevatorMoving());
        }
    }

    private IEnumerator SimulateOpenedDoors()
    {
        _isDoorsOpened = true;
        DidChangeDoorState?.Invoke(true);
        yield return new WaitForSeconds(_openedDoorsSimulationTime);
        _isDoorsOpened = false;
        //_openedDoorsCoroutine = null;
        DidChangeDoorState?.Invoke(false);

        _openedDoorsCoroutine = null;
        if (_commands.Count > 0)
        {
            _currentElevatorDirection = GetElevatorDirection(_commands[0].floor,_currentFloor);
            if (_elevatorSimulationCoroutine == null)
                _elevatorSimulationCoroutine = StartCoroutine(SimulateElevatorMoving());
        }
        else
        {
            _currentElevatorDirection = ElevatorDirection.Nowhere;
            DidFinishedMoving?.Invoke(_currentFloor);
        }
        
    }

    private ElevatorDirection GetElevatorDirection(int newFloor, int oldFloor)
    {
        if (newFloor > oldFloor)
            return ElevatorDirection.Up;
        else if ((newFloor == oldFloor))
            return ElevatorDirection.Nowhere;
        else
            return ElevatorDirection.Down;
    }

}
