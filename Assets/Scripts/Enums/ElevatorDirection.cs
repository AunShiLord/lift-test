namespace Enums
{
    public enum ElevatorDirection
    {
        Up = 1,
        Down = -1,
        Nowhere = 0
    }
}