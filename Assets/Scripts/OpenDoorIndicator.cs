﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenDoorIndicator : MonoBehaviour
{
    
    [SerializeField] 
    private Image _elevatorDoorOpenedimage;
    [SerializeField] 
    private Color _indicatorNormalColor;
    [SerializeField] 
    private Color _indicatorHighlightedColor;
     
    public bool IsDoorOpened
    {
        get { return _isDoorOpened; }
        set
        {
            _isDoorOpened = value;
            if (value)
                _elevatorDoorOpenedimage.color = _indicatorHighlightedColor;
            else
                _elevatorDoorOpenedimage.color = _indicatorNormalColor;
        }
    }

    private bool _isDoorOpened = false;

}
